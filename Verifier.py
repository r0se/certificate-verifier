import OpenSSL
import argparse

''' 
Arguments to add:
    pass the path by argument
    read the doc carefully
'''

class Verifier:

    def __init__(self, path):
        '''
        Constructor of the class. Every instance of the class
        will need to be initialized with a given parameter, poiting
        to the certificate.
        '''
        self.path = path


    def __load_certificate(self):
        self.__cert = OpenSSL.crypto.load_certificate(
                        OpenSSL.crypto.FILETYPE_PEM, open(self.path).read()
        )

        return self.__cert


    def get_signature_algorithm(self):
        self.algorithm = self.__load_certificate().get_signature_algorithm()
        return self.algorithm

verifier = Verifier('C:\\Users\\SubaruSama\\Desktop\\Faculdade\\Sem 5\\Segurança em Comércio Eletrônico\\certificate-verifier\\wwwpythonorg.crt')
print('Signature Algorithm of the Certificate: {}'.
        format(verifier.get_signature_algorithm()
            .decode('utf-8'))
    )